<?php
require "../lib/class.dosen.php";
$dos = new dosen();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="konten-blok">
                <div class="konten-head">Filter Mata Kuliah</div>
                <div class="konten-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Tahun Akademik</label>
                                <input type="number" id="fmk_ta" class="form-control" value="<?=date('Y');?>">
                            </div>
                            <div class="col-sm-3">
                                <label>Semester</label>
                                <input type="number" id="fmk_sm" class="form-control" value="1">
                            </div>
                            <div class="col-sm-3">
                                <label>Dosen</label>
                                <select id="fmk_ds" class="form-control">
                                <?php
                                $dosen = $dos->weare();
                                foreach($dosen as $i=>$data){
                                    echo "<option value={$data['nidn']}>{$data['nama']}</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <br>
                                <button class="btn btn-primary" id="fmk_seek">Cari Mata Kuliah</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="konten-blok">
                <div class="konten-head">
                    Mata Kuliah Diampu
                </div>
                <div class="konten-body">
                    <div class="list-group" id="mka"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="konten-blok">
                <div class="konten-head">Form Penilaian <span id="namaMk"></span></div>
                <div class="konten-body">
                    <form action="aksi-khs.php" method="post">
                        <div id="formNilai" class="form-horizontal"></div>
                        <div class="form-group" id="formNilaiSubmit">
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#fmk_seek").click( function(){
            let ta,sm,ds;
            ta = $("#fmk_ta").val();
            sm = $("#fmk_sm").val();
            ds = $("#fmk_ds").val();

            $.getJSON(`json_mka.php?ta=${ta}&sm=${sm}&ds=${ds}` , function(mka){
                $("#mka li").remove();
                $.each(mka , function(i,data){
                    $("#mka").append(`
                        <li class='list-group-item mka' id=${data.kdmk}>${data.nmmk}</li>
                    `);
                })
            })
        })

        $("#mka").on('click' , '.mka' , function(){
            let ta,sm,ds;
            ta = $("#fmk_ta").val();
            sm = $("#fmk_sm").val();
            ds = $("#fmk_ds").val();
            mk = $(this).prop('id');
            kl = $(this).text();

            $.getJSON(`json_std.php?ta=${ta}&sm=${sm}&ds=${ds}&mk=${mk}` , function(mka){
                /* $("#mka li").remove(); */
                $("#namaMk").html(kl);
                $("#formNilaiSubmit button").remove();
                $("#formNilai div").remove();
                $.each(mka , function(i,data){
                    $("#formNilai").append(`
                    <div class='form-group'>
                    <label class='col-sm-4'>${data.mahasiswa}</label>
                    <div class='col-sm-8'>
                    <input type='number' class='form-control' name='${data.id_krhs}' value='75'>
                    </div>
                    </div>
                    `);
                })
                $("#formNilaiSubmit").append(`
                    <button type="submit" class="btn btn-success">Simpan</button>
                `);
            })
        })
    })
</script>