<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sistem Akademik</title>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/siskadem.css">
</head>
<body>
  <div class="container sa-container">
    <div class="row">
      <div class="col-sm-2">
        <header style="margin:15px auto;">
          <a href="#"><img src="../img/stb-logo.png" alt="Insert Logo Here" width="90" height="90" id="Insert_logo" /></a>
        </header>

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#saNavbar" style="background-color:darkgreen;">
            <span class="icon-bar" style="color:white;"></span>
            <span class="icon-bar" style="color:white;"></span>
            <span class="icon-bar" style="color:white;"></span>
          </button>
          <a class="navbar-brand" href="#">Menu Utama</a>
        </div>
        <div class="collapse navbar-collapse"  id="saNavbar">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="./">Home</a></li>
            <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">DOSEN
              <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="./?data=dfdosen">DAFTAR DOSEN</a></li>                
                <li><a href="./?data=dfkonaj">KONTRAK AJAR</a></li>                
              </ul>
            </li>
            <li><a href="./?data=dfmahasiswa">MAHASISWA</a></li>
            <li><a href="./?data=dfmakul">MATA KULIAH</a></li>
            <li><a href="./?data=dfadmin">ADMIN</a></li>
            <li><a href="./?data=dfuser">USER</a></li>
            <li><a href="./?data=dfprodi">PRODI</a></li>
            <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Administrasi
              <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="./?data=formAdmin">Pembayaran</a></li>
                <li><a href="./?data=dfadministrasi">Laporan</a></li>
                <li><a href="./?data=frbiaya">Biaya</a></li>
              </ul>
            </li>
            <li><a href="./?data=frkrs">KRS</a></li>
            <li><a href="../">Logout</a></li>
          </ul> 
        </div>

          
      </div>
      <div class="col-sm-10">
        <?php include ('konten.php'); ?>
      </div>
    </div>
  </div>
</body>
</html>