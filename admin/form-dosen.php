<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$dosen = new admin();
?>
<div class="page-title">
  <h3>FORMULIR DOSEN</h3>
</div>
  <?php if(!$_GET['id']): ?>
  <form action='aksi-dosen.php' method='POST'>
    <input type="hidden" name="modus" value="baru">
  <?php 
    $dosen->getForm('dosen','i','',1); 
  ?>
  </form>
<?php else: ?>
<form action='aksi-dosen.php' method='POST'>
    <input type="hidden" name="modus" value="ubah">
  <?php 
    $dataDosen = $dosen->dataDosen($_GET['id']);
    $form->getForm('dosen','e',$dataDosen,1); 
  ?>
  </form>
<?php endif; ?>
