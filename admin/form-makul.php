<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$makul = new admin();
?>

<div class="page-title">
  <h3>FORMULIR MATA KULIAH</h3>
</div>
  <?php if(!$_GET['id']): ?>
  <form action='aksi-makul.php' method='POST'>
    <input type="hidden" name="modus" value="baru">
  <?php 
    $form->getForm('makul','i','',1); 
  ?>
  </form>
<?php else: ?>
<form action='aksi-makul.php' method='POST'>
    <input type="hidden" name="modus" value="ubah">
  <?php 
    $data = $makul->dataMakul($_GET['id']);
    $form->getForm('makul','e',$data,1); 
  ?>
  </form>
<?php endif; ?>
