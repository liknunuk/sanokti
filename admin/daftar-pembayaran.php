<?php
require('../lib/class.admin.php');
$admin = new admin();
if(!$_GET['bl']){
    $bulanIni = date('Y-m');
}else{
    $bulanIni = $_GET['bl'];
}

$bayar = $admin->lapbayar($bulanIni);
?>

<div class="page-title">
	<h3>DAFTAR PEMBAYARAN</h3>
</div>

<table class="table table-bordered">
    <thead>
        <tr>
            <td>Tanggal</td>
            <td>NIM</td>
            <td>mahasiswa</td>
            <td>jumlah</td>
            <td>No. Nota</td>
            <td>Admin</td>
        </tr> 
    </thead>
    <tbody>
    <?php foreach($bayar AS $data): ?>
        <tr>
            <td>
            <?php
            list($h,$b,$t) = explode("-" , $data['tanggal']);
            $blbayar = "$t-$b";
            echo "<a href='./?data=dfadministrasi&bl=$blbayar'>" . $data['tanggal'] ."</a>";
            ?>
            </td>
            <td>
                <a href='./?data=hsadministrasi&nim=<?=$data['nim'];?>'>
                    <?=$data['nim'];?>
                </a>
                </td>
            <td><?=$data['mahasiswa'];?></td>
            <td align='right'><?=number_format($data['jumlah'],0,',','.');?></td>
            <td><?=$data['id_biaya'];?></td>
            <td><?=$data['admin'];?></td>
    <?php endforeach; ?>
    </tbody>
</table>