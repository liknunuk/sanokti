daftar user
<?php
require('../lib/class.admin.php');
$prodi = new admin();

$data = $prodi->daftarprodi();
?>
<div class="page-title">
	<h3>DAFTAR PROGRAM STUDI</h3>
</div>
<a class="btn btn-primary" href="./?data=frprodi">+ Program Studi</a>
<table class="table table-sm table-bordered">
  <thead>
	<tr>
		<th>KODE</th>
		<th>NAMA PROGRAM STUDI</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($data) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".strtoupper($data[$i]['id_prodi'])."</td>
		<td>".strtoupper($data[$i]['nama'])."</td>
		<td>
			<a href='./?data=frprodi&id=".$data[$i]['id_prodi']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$data[$i]['id_prodi']."')>Hapus</a>
		</td>
	  </tr>
	  ";
	  
  }
  
  ?>
  </tbody>
</table>


<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=prodi&id='+id;
		}
	}
</script>