<?php

require("../lib/class.admin.php");
$admin=new admin();

if($_GET['obj'] == 'dosen'){
    $admin->hapusDosen($_GET['id']);
    $returnPage = './?data=dfdosen';
}

if($_GET['obj'] == 'mahasiswa'){
    $admin->hapusMhs($_GET['id']);
    $returnPage = './?data=dfmahasiswa';
}

if($_GET['obj'] == 'makul'){
    $admin->hapusMakul($_GET['id']);
    $returnPage = './?data=dfmakul';
}

if($_GET['obj'] == 'admin'){
    $admin->hapusAdmin($_GET['id']);
    $returnPage = './?data=dfadmin';
}

if($_GET['obj'] == 'user'){
    $admin->hapusUser($_GET['id']);
    $returnPage = './?data=dfuser';
}

if($_GET['obj'] == 'prodi'){
    $admin->hapusProdi($_GET['id']);
    $returnPage = './?data=dfprodi';
}

header("location:".$returnPage);