<?php
require('../lib/class.admin.php');
$user = new admin();

$user = $user->daftaruser();
?>
<div class="page-title">
	<h3>DAFTAR USER</h3>
</div>
<a class="btn btn-primary" href="./?data=fruser">+ user</a>
<table class="table table-bordered table-sm">
  <thead>
	<tr>
		<th>USERNAME</th>
		<th>ID_USER</th>
		<th>PASWORD</th>
		<th>POSISI</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($user) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".$user[$i]['user_name']."</td>
		<td>".$user[$i]['id_user']."</td>
		<td>".$user[$i]['password']. "</td>
		<td>".$user[$i]['posisi']. "</td>
		<td>
			<a href='./?data=fruser&id=".$user[$i]['id_user']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$user[$i]['id_user']."')>Hapus</a>
		</td>
	  </tr>
	  ";
	  
  }
  
  ?>
  </tbody>
</table>

<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=user&id='+id;
		}
	}
</script>