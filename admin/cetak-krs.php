<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KRS</title>
    <style>
    table > tbody > tr > td { font-family: monospace; font-size: 12px; }
    </style>
</head>
<body>
    
</body>
</html>

<?php

require('../lib/class.admin.php');
$admin = new admin();

$krs = $admin->getKrs($_GET['id']);
list($nim,$smt) = explode("-" , $krs[0]['kdKrs']);
$mhs = $admin->dataMhs($nim);
$mkl = explode(",",$krs[0]['mk']);

echo "
    <center><h4>Kartu Rencana Studi</h4></center>
    <table width='600' align='center' border='1' cellspacing='0' cellpadding='4'>
        <tr>
            <td width='225'>Tahun Akademik</td><td>{$krs[0]['thAkademik']}</td>
        </tr>
        <tr>
            <td>Nama Mahasiswa</td><td>{$mhs['nama']}</td>
        </tr>
        <tr>
            <td>NIM</td><td>{$nim}</td>
        </tr>
        <tr>
            <td>Semester</td><td>{$smt}</td>
        </tr>
    </table>
    <br>
";

echo "
<table width='600' align='center' border='1' cellspacing='0' cellpadding='4'>
<tr><th>No.</th><th>Mata Kuliah</th><th>SKS</th></tr>
";
for( $i = 0 ; $i < COUNT($mkl) ; $i++ ){
    $nomor = $i + 1;
    $mk = $admin->dataMakul($mkl[$i]);
    echo "
        <tr><td>{$nomor}</td><td>{$mk['makul']}</td><td align='right'>{$mk['sks']}</td>
    ";
}
echo "
</table>
";