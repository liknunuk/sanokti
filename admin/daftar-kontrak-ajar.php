<?php 
    require "../lib/class.admin.php"; 
    $adm = new admin(); 

    if(!$_GET['id']){
        $modus = "baru";
        $kk = "";
        $ta = date('Y');
        $sm = "";
        $mk = "";
        $ds = "";
    }else{
        $modus = "ubah";
        $kk = $_GET['id'];
        $dk = $adm->pickone("*","kontrak_ngajar","id_kontrak",$kk);
        $ta = $dk['th_akademik'];
        $sm = $dk['semester'];
        $mk = $dk['kode_makul'];
        $ds = $dk['nidn'];
    }
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h3>KONTRAK MENGAJAR DOSEN</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form action="aksi-kontrak-ajar.php" method="post" class="form-inline">
                <input type="hidden" name="modus" value="<?=$modus;?>">
                <input type="hidden" name="kk" value="<?=$kk;?>"> 

                <div class="form-group">
                    <input type="number" name="ta" id="fka_ta" class="form-control" placeholder="Tahun Akademik" value="<?=date('Y');?>">    
                </div>

                <div class="form-group">
                    <input type="text" name="sm" id="fka_sm" class="form-control" placeholder="Semester" value="<?=$sm;?>" >    
                </div>

                <div class="form-group">
                    <input type="text" name="mk" id="fka_mk" class="form-control" placeholder="Kode Mata Kuliah" value="<?=$mk;?>">    
                </div>

                <div class="form-group">
                    <input type="text" name="ds" id="fka_ds" class="form-control" placeholder="NIDN"value="<?=$ds;?>" >    
                </div>

                <div class="form-group">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                </div>

            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php
            $y = date('Y');
            $m = date('m');

            if ($m >= 7){ $y = $y - 0; } else { $y = $y-1; }
            $ktaj = $adm->getKontrakAjar($y);
            
            ?>
            <div class="text-center">
                <h4>DAFTAR KONTRAK MENGAJAR TAHUN AKADEMIK <?=$y;?></h4>
            </div>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Semester</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Dosen</th>
                            <th>Kontrol</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($ktaj AS $ik=>$ka): ?>
                    <tr>
                        <td><?=$ka['semester'];?></td>
                        <td><?=$ka['makul'];?></td>
                        <td><?=$ka['sks'];?></td>
                        <td><?=$ka['nama'];?>, <?=$ka['gelar'];?></td>
                        <td><a href="./?data=dfkonaj&id=<?=$ka['id_kontrak'];?>">edit</a></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>