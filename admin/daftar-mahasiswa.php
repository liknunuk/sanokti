<?php
require('../lib/class.admin.php');
$admin = new admin();

$mhs = $admin->daftarMhs();
?>
<div class="page-title">
	<h3>DAFTAR MAHASISWA</h3>
</div>
<a class="btn btn-primary" href="./?data=frmahasiswa">+ MAHASISWA</a>
<table class="table table-bordered table-sm">
  <thead>
	<tr>
		<th>NIM</th>
		<th>NAMA</th>
		<th>ID PRODI</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($mhs) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".$mhs[$i]['nim']."</td>
		<td>".$mhs[$i]['nama']."</td>
		<td>".$mhs[$i]['id_prodi']. "</td>
		<td>
			<a href='./?data=frmahasiswa&id=".$mhs[$i]['nim']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$mhs[$i]['nim']."')>Hapus</a>
		</td>
	  </tr>
	  ";
	  
  }
  
  ?>
  </tbody>
</table>
<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=mahasiswa&id='+id;
		}
	}
</script>