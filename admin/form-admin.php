<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$admin = new admin();
?>

<div class="page-title">
  <h3>FORMULIR MATA KULIAH</h3>
</div>
  <?php if(!$_GET['id']): ?>
  <form action='aksi-admin.php' method='POST'>
    <input type="hidden" name="modus" value="baru">
  <?php 
    $form->getForm('admin','i','',1); 
  ?>
  </form>
<?php else: ?>
<form action='aksi-admin.php' method='POST'>
    <input type="hidden" name="modus" value="ubah">
  <?php 
    $data = $admin->dataAdmin($_GET['id']);
    $form->getForm('admin','e',$data,1); 
  ?>
  </form>
<?php endif; ?>
