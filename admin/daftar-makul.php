
<?php

require('../lib/class.admin.php');
$admin = new admin();

$makul = $admin->daftarMakul();

?>
<div class="page-title">
	<h3>DAFTAR MATA KULIAH</h3>
</div>
<a class="btn btn-primary" href="./?data=frmakul">+ MATA KULIAH</a>
<table class="table table-bordered table-sm">
  <thead>
	<tr>
		<th>Kode MK</th>
		<th>Nama MK</th>
		<th>SKS</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($makul) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".$makul[$i]['kode_makul']."</td>
		<td>".$makul[$i]['makul']."</td>
		<td>".$makul[$i]['sks']."</td>
		<td>
			<a href='./?data=frmakul&id=".$makul[$i]['kode_makul']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$makul[$i]['kode_makul']."')>Hapus</a>
		</td>
	  </tr>
	  ";
	  
  }
  
  ?>
  </tbody>
</table>

<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=makul&id='+id;
		}
	}
</script>