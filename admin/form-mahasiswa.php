<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$mhs = new admin();
?>

<div class="page-title">
  <h3>FORMULIR MAHASISWA</h3>
</div>
  <?php if(!$_GET['id']): ?>
  <form action='aksi-mahasiswa.php' method='POST'>
    <input type="hidden" name="modus" value="baru">
  <?php 
    $mhs->getForm('mahasiswa','i','',1); 
  ?>
  </form>
<?php else: ?>
<form action='aksi-mahasiswa.php' method='POST'>
    <input type="hidden" name="modus" value="ubah">
  <?php 
    $data = $mhs->dataMhs($_GET['id']);
    $form->getForm('mahasiswa','e',$data,1); 
  ?>
  </form>
<?php endif; ?>
