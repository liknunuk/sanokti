<?php 
session_start(); 

require("../lib/class.admin.php");
require("../lib/terbilang.php");
$admin=new admin();

$nota = $_SESSION['nota'];
/*
    [nim] => S1TISI30187
    [semest( r] => 9
    [tgl_pembayaran] => 2019-09-08
    [jumlah] => 2500000
    [nip] => 123
    [id_biaya] => 1909080001
*/
$mhs = $admin->dataMhs($nota['nim']);
$adm = $admin->dataAdmin($nota['nip']);
$prodi = $admin->dataProdi($mhs['id_prodi']);
$trbil = terbilang($nota['jumlah']);

list($th,$bl,$hr) = explode( '-' , $nota['tgl_pembayaran']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota Pembayaran</title>
    <style>
        p{margin:0;padding:0}
        @media print{
            .takecetak{ display:none;}
        }
    </style>
</head>
<body onload=window.print()>
<h2>Tanda Terima Pembayaran</h2>
<p>Telah terima dari : <?=$mhs['nama'];?> ( NIM: <?=$mhs['nim'];?> )</p>
<p>Uang Sejumlah : Rp <?=number_format($nota['jumlah'],2,',','.');?></p>
<p>Guna Membayar : Biaya administrasi kuliah Program Studi <?=$prodi['nama'];?> Semester <?=$nota['semester'];?></p>
<p>Terbilang : <i><?=$trbil;?> rupiah</i></p>
<br><br>
Banjarnegara, <?=$hr.' - '.$bl.' - '.$th;?>
<br><br><br>(__<?=strtoupper($adm['nama']);?>__)


<a href="./?data=formAdmin" class="takecetak">Kembali</a>
<?php unset($_SESSION['nota']);?>
</body>
</html>