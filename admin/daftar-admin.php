<?php
require('../lib/class.admin.php');
$user = new admin();

$data = $user->daftaradmin();
?>
<div class="page-title">
	<h3>DAFTAR PETUGAS ADMIN</h3>
</div>
<a class="btn btn-primary" href="./?data=fradmin">+ admin</a>
<table class="table table-sm table-bordered">
  <thead>
	<tr>
		<th>NIP</th>
		<th>NAMA</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($data) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".$data[$i]['nip']."</td>
		<td>".$data[$i]['nama']."</td>
		<td>
			<a href='./?data=fradmin&id=".$data[$i]['nip']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$data[$i]['nip']."')>Hapus</a>
		</td>
	  </tr>
	  ";
	  
  }
  
  ?>
  </tbody>
</table>

<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=admin&id='+id;
		}
	}
</script>