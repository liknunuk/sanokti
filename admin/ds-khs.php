<?php

require('../lib/class.admin.php');
$admin = new admin();
// $cekKRS = $krs->cekLunas('S1TISI30187',10);
// echo json_encode($cekKRS);
// SELECT krs.kode_makul , makul.makul , krs.bobot , krs.nilai FROM krs , makul WHERE krs.id_kontrak = 'S1TISI30187-11' && makul.kode_makul = krs.kode_makul

$khs = $admin->picksome("krs.kode_makul , makul.makul , krs.bobot , krs.nilai" , "krs , makul" , "krs.id_kontrak = '{$_GET['idk']}' && makul.kode_makul = krs.kode_makul");

$dataKhs = [];
$tsxn = 0; $tsks = 0;
foreach($khs as $i=>$data){
    $sxn = $data['bobot'] * $data['nilai'];
    $result = ['kmk'=>$data['kode_makul'],'nmk'=>$data['makul'],'sks'=>$data['bobot'],'nil'=>sprintf("%.2f",$data['nilai']),'sxn'=>sprintf("%.2f",$sxn)];
    array_push($dataKhs,$result);
    $tsxn += $sxn;
    $tsks += $data['bobot'];
}
$ips = $tsxn / $tsks;
$report = [ 'result' => $dataKhs , 'tsxn'=>sprintf("%.2f",$tsxn),'tsks'=>$tsks , 'ips'=>sprintf("%.2f",$ips)];
echo json_encode($report);

// echo $tsxn . "<br>";
// echo $tsks . "<br>";
// echo $ips . "<br>";
// echo sprintf("%.3f",$ips) . "<br>";


