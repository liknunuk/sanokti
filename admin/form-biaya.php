<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$biaya = new admin();

if(!$_GET['id']){
    $modus = "baru";
    $idx = '';
    $tak = date('Y');
    $smt = 5;
    $jml = 500000;

}else{
    $modus = "ubah";
    $data = $biaya->databiaya($_GET['id']);
    $idx = $_GET['id'];
    $tak = $data['th_akademik'];
    $smt = $data['semester'];
    $jml = $data['jumlah'];
}
?>
<div class="page-title">
  <h3>FORMULIR BEBAN BIAYA</h3>
</div>
<div class="container-fluid">
    <!-- form biaya -->
    <div class="row">
        <div class="col-lg-12">
            <form action="aksi-biaya.php" method="post" class="form-horizontal bebi-form">
                <!-- th_akademik , semester , jurusan , jumlah , satuan -->
                <input type="hidden" name="modus" value=<?=$modus;?>>
                <div class="form-group">
                    <label for="idx" class="col-sm-4">No. Index</label>
                    <div class="col-sm-8">
                        <input type="number" name="idx" id="idx" class="form-control" readonly value=<?=$idx;?> >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4" for="th_akademik">Tahun Akademik</label>
                    <div class="col-sm-8">
                        <input type="number" name="th_akademik" id="th_akademik" class="form-control" value=<?=date('Y');?> value=<?=$tak;?>>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4" for="semester">Semester</label>
                    <div class="col-sm-8">
                        <input type="number" name="semester" id="semester" class="form-control" min=1 max=16 value=<?=$smt;?>>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4" for="jurusan">Program Studi</label>
                    <div class="col-sm-8">
                        <select name="jurusan" id="jurusan" class="form-control">
                            <option value="si">Sistem Informasi</option>
                            <option value="ti">Teknik Informatika</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4" for="jumlah">Besaran Biaya</label>
                    <div class="col-sm-8">
                        <input type="number" name="jumlah" id="jumlah" class="form-control" value=<?=$jml;?>>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4" for="satuan">Satuan Biaya</label>
                    <div class="col-sm-8">
                        <select name="satuan" id="satuan" class="form-control">
                            <option value="per bulan">Per Bulan</option>
                            <option value="per sks">Per SKS</option>
                            <option value="per semester">per semester</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div style="text-align:right; padding-right: 5px;">
                        <input type="submit" value="Simpan" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- beban biaya -->
    <div class="row">
        <div class="col-lg-12">
            <h3>DAFTAR BIAYA KULIAH</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="100">Tahun<br>Akademik</th>
                        <th width="100">Semester</th>
                        <th>Jurusan</th>
                        <th>Jumlah</th>
                        <th>Satuan</th>
                        <th width="120">Kontrol</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $ib = $biaya->daftarbiaya();
                        foreach($ib AS $bebi):
                        ?>
                        <tr>
                            <td><?=$bebi['th_akademik'];?></td>
                            <td><?=$bebi['semester'];?></td>
                            <td><?=strtoupper($bebi['prodi']);?></td>
                            <td align='right'><?=number_format($bebi['jumlah'],0,',','.');?></td>
                            <td><?=ucfirst($bebi['satuan']);?></td>
                            <td>
                                <a href="./?data=frbiaya&id=<?=$bebi['idx'];?>">Edit</a> | 
                                <a href=javascript:void(0) onClick=hapus("<?=$bebi['idx'];?>")>Hapus</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        
        </div>
    </div>
</div>

<script>
function hapus(idx){
    let tenan = confirm("Data akan dihapus !!!");
    if( tenan === true ){
        $.post('aksi-biaya.php',{
            modus: 'hapus',
            idx : idx
        },function(){
            location.reload();
        })
    }
}
</script>