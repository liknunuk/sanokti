<?php
require("../lib/class.forms.inc.php");
require("../lib/class.admin.php");
$form = new forms();
$user = new admin();
?>

<div class="page-title">
  <h3>FORMULIR USER</h3>
</div>
  <?php if(!$_GET['id']): ?>
  <form action='aksi-user.php' method='POST'>
    <input type="hidden" name="modus" value="baru">
  <?php 
    $form->getForm('user','i','',1); 
  ?>
  </form>
<?php else: ?>
<form action='aksi-user.php' method='POST'>
    <input type="hidden" name="modus" value="ubah">
  <?php 
    $data = $user->dataUser($_GET['id']);
    $form->getForm('user','e',$data,1); 
  ?>
  </form>
<?php endif; ?>
