<?php
require('../lib/class.admin.php');
$admin = new admin();
$dosen = $admin->daftarDosen();
?>
<div class="page-title">
	<h3>DAFTAR DOSEN</h3>
</div>
<a class="btn btn-primary" href="./?data=frdosen">+ DOSEN</a>
<table class="table table-bordered table-sm">
  <thead>
	<tr>
		<th>NAMA DOSEN</th>
		<th>GELAR</th>
		<th>NIDN</th>
		<th>EMAIL</th>
		<th>KONTROL</th>
	</tr>
  </thead>
  <tbody>
  <?php
  for($i = 0 ; $i < COUNT($dosen) ; $i++ ){
	  
	  echo "
	  <tr>
		<td>".$dosen[$i]['nama']."</td>
		<td>".$dosen[$i]['gelar']."</td>
		<td>".$dosen[$i]['nidn']."</td>
		<td>".$dosen[$i]['email']."</td>
		<td>
			<a href='./?data=frdosen&id=".$dosen[$i]['nidn']."'>Edit</a> | 
			<a href='javascript:void(0)' onClick = hapus('".$dosen[$i]['nidn']."')>Hapus</a>
		</td>
	  </tr>
	  ";  
  }
  
  ?>
  </tbody>
</table>
<script>
	function hapus(id){
		let tenan = confirm('Data akan dihapus !!!');
		if( tenan == true ) {
			window.location = './hapus.php?obj=dosen&id='+id;
		}
	}
</script>