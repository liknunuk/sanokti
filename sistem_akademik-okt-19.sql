-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sistem_akademik
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_baak`
--

DROP TABLE IF EXISTS `admin_baak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_baak` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_baak`
--

LOCK TABLES `admin_baak` WRITE;
/*!40000 ALTER TABLE `admin_baak` DISABLE KEYS */;
INSERT INTO `admin_baak` VALUES ('stba001','yuniar'),('stba002','leli'),('stba003','Munarman');
/*!40000 ALTER TABLE `admin_baak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `administrasi`
--

DROP TABLE IF EXISTS `administrasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrasi` (
  `nim` varchar(11) NOT NULL,
  `semester` int(2) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `nip` int(10) NOT NULL,
  `id_biaya` varchar(10) NOT NULL,
  PRIMARY KEY (`id_biaya`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrasi`
--

LOCK TABLES `administrasi` WRITE;
/*!40000 ALTER TABLE `administrasi` DISABLE KEYS */;
INSERT INTO `administrasi` VALUES ('S1TISI30187',9,'2019-09-08',750000,0,'1909080001'),('S1TISI30187',9,'2019-07-10',750000,0,'1907100001'),('S1TISI30187',9,'2019-08-10',750000,0,'1908100001'),('S1TISI30187',9,'2019-09-10',210000,0,'1909100001'),('S1TISI30187',10,'2019-09-15',2500000,0,'1909150001'),('S1TISI30182',10,'2019-09-15',2500000,0,'1909150002'),('S1TISI30182',11,'2019-09-30',2500000,0,'1909300001'),('S1TISI30187',11,'2019-09-30',2500000,0,'1909300002');
/*!40000 ALTER TABLE `administrasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beban_biaya`
--

DROP TABLE IF EXISTS `beban_biaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beban_biaya` (
  `idx` int(3) NOT NULL AUTO_INCREMENT,
  `th_akademik` int(4) NOT NULL,
  `semester` int(2) NOT NULL,
  `jurusan` varchar(2) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` enum('per bulan','per sks','per semester') NOT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beban_biaya`
--

LOCK TABLES `beban_biaya` WRITE;
/*!40000 ALTER TABLE `beban_biaya` DISABLE KEYS */;
INSERT INTO `beban_biaya` VALUES (1,2019,1,'si',2500000,'per semester'),(2,2019,2,'si',2500000,'per semester'),(3,2019,3,'si',2500000,'per semester'),(4,2019,4,'si',2500000,'per semester'),(5,2019,5,'si',2500000,'per semester'),(6,2019,6,'si',2500000,'per semester'),(7,2019,7,'si',2500000,'per semester'),(8,2019,8,'si',2500000,'per semester'),(9,2019,9,'si',2500000,'per semester'),(10,2019,10,'si',2500000,'per semester'),(11,2019,10,'ti',410000,'per bulan'),(12,2019,9,'ti',410000,'per bulan'),(13,2019,8,'ti',410000,'per bulan'),(14,2019,7,'ti',410000,'per bulan'),(15,2019,6,'ti',410000,'per bulan'),(16,2019,5,'ti',410000,'per bulan'),(17,2019,4,'ti',425000,'per bulan'),(18,2019,3,'ti',425000,'per bulan'),(19,2019,2,'ti',425000,'per bulan'),(20,2019,1,'ti',425000,'per bulan'),(22,2019,11,'ti',410000,'per bulan');
/*!40000 ALTER TABLE `beban_biaya` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen` (
  `nama` varchar(30) NOT NULL,
  `gelar` varchar(5) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nidn` int(10) NOT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen`
--

LOCK TABLES `dosen` WRITE;
/*!40000 ALTER TABLE `dosen` DISABLE KEYS */;
INSERT INTO `dosen` VALUES ('Fajar JEP','M.Kom','fajar@gmail.com',123),('Dodit Mulyadi','S.SOS','mulyadisos@gmail.co.loid',11223),('Uni Sofia Et','M.Buh','soviet@rusia.ru',11221);
/*!40000 ALTER TABLE `dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_iface`
--

DROP TABLE IF EXISTS `form_iface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_iface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_iface`
--

LOCK TABLES `form_iface` WRITE;
/*!40000 ALTER TABLE `form_iface` DISABLE KEYS */;
INSERT INTO `form_iface` VALUES (1,'dosen','NIDN','nidn','text','nidn',NULL),(2,'dosen','Nama Dosen','nama','text','nama',NULL),(4,'dosen','Gelar','gelar','text','gelar',NULL),(5,'dosen','Email','email','text','email',NULL),(12,'mahasiswa','NIM','nim','text','nim',NULL),(13,'mahasiswa','Nama Mahasiswa','nama','text','nama',NULL),(14,'mahasiswa','Program Studi','kodeprodi','select','kodeprodi','optValue/optLabel/form_opsi'),(15,'makul','Kode Mata Kuliah','kode_makul','text','kode_makul',''),(16,'makul','Nama Mata Kuliah','makul','text','makul',''),(17,'makul','Beban SKS','sks','text','sks',''),(18,'admin','Nomor Induk Pegawai','nip','text','nip',''),(19,'admin','Nama Pegawai','nama','text','nama',''),(32,'prodi','Program Studi','id_prodi','text','id_prodi',''),(33,'prodi','Nama Studi','nama','text','nama',''),(41,'user','Username','user_name','text','user_name',''),(42,'user','Id User','id_user','text','id_user',''),(43,'user','Password','pass','password','pass',''),(44,'user','Posisi','posisi','select','posisi','optValue/optLabel/form_opsi'),(45,'administrasi','Nomor Induk Mahasiswa','nim','text','nim',''),(46,'administrasi','Semester','semester','text','semester',''),(47,'administrasi','Tanggal Pembayaran','tgl_pembayaran','date','tgl_bayar',''),(48,'administrasi','Jumlah','jumlah','text','jumlah',''),(49,'administrasi','Petugas Adminisrasi','nip','select','nip','nip/nama/admin_baak'),(50,'administrasi','Nomor Nota','id_biaya','text','id_biaya','readonly');
/*!40000 ALTER TABLE `form_iface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_opsi`
--

DROP TABLE IF EXISTS `form_opsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_opsi` (
  `idx_pilihan` int(2) NOT NULL AUTO_INCREMENT,
  `optGroup` varchar(20) DEFAULT NULL,
  `optValue` varchar(20) DEFAULT NULL,
  `optLabel` text DEFAULT NULL,
  PRIMARY KEY (`idx_pilihan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_opsi`
--

LOCK TABLES `form_opsi` WRITE;
/*!40000 ALTER TABLE `form_opsi` DISABLE KEYS */;
INSERT INTO `form_opsi` VALUES (1,'kodeprodi','si','sistem informasi'),(2,'kodeprodi','ti','teknik informatika'),(3,'posisi','mahasiswa','MAHASISWA'),(4,'posisi','dosen','DOSEN'),(5,'posisi','admin','ADMIN');
/*!40000 ALTER TABLE `form_opsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kkrs`
--

DROP TABLE IF EXISTS `kkrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kkrs` (
  `thAkademik` int(4) DEFAULT NULL,
  `kdKrs` varchar(20) NOT NULL,
  `mk` tinytext DEFAULT NULL,
  PRIMARY KEY (`kdKrs`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kkrs`
--

LOCK TABLES `kkrs` WRITE;
/*!40000 ALTER TABLE `kkrs` DISABLE KEYS */;
INSERT INTO `kkrs` VALUES (2019,'S1TISI30187-10','TI015,TI051,TI027,TI052,TI040,TI013,TI001');
/*!40000 ALTER TABLE `kkrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kontrak_ngajar`
--

DROP TABLE IF EXISTS `kontrak_ngajar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kontrak_ngajar` (
  `th_akademik` varchar(4) NOT NULL,
  `semester` varchar(2) NOT NULL,
  `kode_makul` varchar(5) NOT NULL,
  `id_kontrak` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `nidn` int(6) NOT NULL,
  PRIMARY KEY (`id_kontrak`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kontrak_ngajar`
--

LOCK TABLES `kontrak_ngajar` WRITE;
/*!40000 ALTER TABLE `kontrak_ngajar` DISABLE KEYS */;
INSERT INTO `kontrak_ngajar` VALUES ('2019','11','TI011',1,123),('2019','11','TI002',2,11223),('2019','11','TI003',3,11221),('2019','9','TI001',4,11223);
/*!40000 ALTER TABLE `kontrak_ngajar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `krs`
--

DROP TABLE IF EXISTS `krs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `krs` (
  `nim` varchar(11) NOT NULL,
  `semester` varchar(2) NOT NULL,
  `kode_makul` varchar(5) NOT NULL,
  `th_akademik` varchar(4) NOT NULL,
  `nilai` float(4,2) DEFAULT NULL,
  `id_kontrak` varchar(20) DEFAULT NULL,
  `bobot` int(1) NOT NULL,
  `id_krhs` varchar(14) NOT NULL,
  `keterangan` enum('baru','mengulang') NOT NULL,
  PRIMARY KEY (`id_krhs`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `krs`
--

LOCK TABLES `krs` WRITE;
/*!40000 ALTER TABLE `krs` DISABLE KEYS */;
INSERT INTO `krs` VALUES ('S1TISI30187','11','TI002','2019',3.00,'S1TISI30187-11',2,'S1TISI30187001','baru'),('S1TISI30187','11','TI003','2019',2.96,'S1TISI30187-11',4,'S1TISI30187002','baru'),('S1TISI30187','11','TI011','2019',3.04,'S1TISI30187-11',3,'S1TISI30187003','baru'),('S1TISI30182','11','TI002','2019',3.04,'S1TISI30182-11',2,'S1TISI30182001','baru'),('S1TISI30182','11','TI003','2019',2.96,'S1TISI30182-11',4,'S1TISI30182002','baru'),('S1TISI30182','11','TI011','2019',3.00,'S1TISI30182-11',3,'S1TISI30182003','baru');
/*!40000 ALTER TABLE `krs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mahasiswa` (
  `nim` varchar(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `id_prodi` varchar(2) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mahasiswa`
--

LOCK TABLES `mahasiswa` WRITE;
/*!40000 ALTER TABLE `mahasiswa` DISABLE KEYS */;
INSERT INTO `mahasiswa` VALUES ('S1TISI30187','NOKTI','ti'),('S1TISI30182','Kingkin Melyana','ti');
/*!40000 ALTER TABLE `mahasiswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `makul`
--

DROP TABLE IF EXISTS `makul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `makul` (
  `kode_makul` varchar(5) NOT NULL,
  `makul` varchar(30) NOT NULL,
  `sks` int(1) NOT NULL,
  PRIMARY KEY (`kode_makul`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `makul`
--

LOCK TABLES `makul` WRITE;
/*!40000 ALTER TABLE `makul` DISABLE KEYS */;
INSERT INTO `makul` VALUES ('A001','BAHASA INGGRIS LANJUT',2),('TI001','PENDIDIKAN AGAMA*',2),('TI002','BAHASA INGGRIS INFORMATIKA',2),('TI003','TEKNISI HARDWARE',4),('TI004','PENGANTAR TEKNOLOGI INFORMASI*',2),('TI006','ALGORITMA DAN PEMROGRAMAN*',4),('TI007','KALKULUS*',4),('TI008','LOGIKA INFORMATIKA*',2),('TI009','TEORI BAHASA OTOMATA',2),('TI011','ALJABAR LINEAR*',3),('TI012','KONSEP SISTEM INFORMASI*',3),('TI013','SISTEM BASIS DATA',2),('TI015','PEMROGRAMAN PROSEDURAL',4),('TI016','PENDIDIKAN PANCASILA*',2),('TI017','PRAKTIKUM BASIS DATA',2),('TI020','MATEMATIKA DISKRIT',2),('TI021','METODOLOGI PENGEMBANGAN S.I',2),('TI022','SISTEM OPERASI*',2),('TI024','PENDIDIKAN KEWARGANEGARAAN*',2),('TI025','ARSITEKTUR DAN ORGANISASI KOMP',3),('TI026','NETWORK PROTOCOL',4),('TI027','PEMROGRAMAN WEB DATABASE',4),('TI029','APLIKASI KOMPUTER',4),('TI030','SISTEM TERINTEGRASI',4),('TI033','METODOLOGI RISET TEKNOLOGI INF',2),('TI034','REKAYASA PERANGKAT LUNAK',4),('TI035','KECERDASAN BUATAN',3),('TI036','PEMROGRAMAN BERORIENTASI OBJEK',4),('TI037','KEWIRAUSAHAAN',2),('TI038','KOMPUTASI CLIENT SERVER',2),('TI039','KECAKAPAN ANTAR PERSONAL*',2),('TI040','SIMULASI DAN PEMODELAN',2),('TI041','INTERAKSI MANUSIA DAN KOMPUTER',2),('TI042','KOMPUTER DAN MASYARAKAT',2),('TI043','TEKNOLOGI JARINGAN',3),('TI044','SISTEM PAKAR',4),('TI047','ETIKA PROFESI',2),('TI048','KERJA PRAKTEK',2),('TI049','OBJECT ORIENTED AND ANALISYS D',4),('TI050','TUGAS AKHIR',6),('TI051','PEMROGRAMAN WEB DASAR',2),('TI052','PEMROGRAMAN WEB LANJUT',2),('TI053','PEMROGRAMAN DATABASE',3),('TI054','JARINGAN KOMPUTER*',4),('TI055','STRUKTUR DATA',3),('TI056','ANALISA DAN PERANCANGAN S.I*',4),('TI057','SISTEM TELEKOMUNIKASI',3),('TI058','JARINGAN KOMPUTER LANJUT',4),('TI059','KEAMANAN KOMPUTER',3),('TI060','GUI PROGRAMMING',3),('TI061','SISTEM ADMINISTRASI JARINGAN',3);
/*!40000 ALTER TABLE `makul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodi`
--

DROP TABLE IF EXISTS `prodi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodi` (
  `id_prodi` varchar(2) NOT NULL,
  `nama` varchar(30) NOT NULL,
  PRIMARY KEY (`id_prodi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodi`
--

LOCK TABLES `prodi` WRITE;
/*!40000 ALTER TABLE `prodi` DISABLE KEYS */;
INSERT INTO `prodi` VALUES ('si','Sistem Informasi'),('ti','Teknik Informatika');
/*!40000 ALTER TABLE `prodi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_name` varchar(11) NOT NULL,
  `id_user` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `posisi` varchar(10) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('leli','stba001','4bdcad851d77e29c1e6f2f981a52d0ae','admin'),('yuniar','stba002','38eba34c49a4d328c55c8331702a2fc6','admin'),('munarman','stba003','bacb633ed4d3eeb4979c9b4c140577a3','admin'),('fajarjep','123','1a605250c3bf02391eca57f8f272c3f2','dosen'),('domul','11223','7930b1223132e901286a6647678fe2eb','dosen'),('soviet','11222','2cefbd41dbd78c0f971bc66ccb835d94','dosen'),('melya','S1TISI30182','3cac7b5d64b3f515ec00b636e9e6a937','mahasiswa'),('nokti','S1TISI30187','6c8ea2d9249c9eb15b224ba7882873e1','mahasiswa');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `v_kbm`
--

DROP TABLE IF EXISTS `v_kbm`;
/*!50001 DROP VIEW IF EXISTS `v_kbm`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `v_kbm` (
  `id_krhs` tinyint NOT NULL,
  `id_kontrak` tinyint NOT NULL,
  `th_akademik` tinyint NOT NULL,
  `semester` tinyint NOT NULL,
  `nidn` tinyint NOT NULL,
  `dosen` tinyint NOT NULL,
  `kode_makul` tinyint NOT NULL,
  `makul` tinyint NOT NULL,
  `nim` tinyint NOT NULL,
  `mahasiswa` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_kbm`
--

/*!50001 DROP TABLE IF EXISTS `v_kbm`*/;
/*!50001 DROP VIEW IF EXISTS `v_kbm`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`nokti`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_kbm` AS select `krs`.`id_krhs` AS `id_krhs`,`krs`.`id_kontrak` AS `id_kontrak`,`krs`.`th_akademik` AS `th_akademik`,`krs`.`semester` AS `semester`,`dosen`.`nidn` AS `nidn`,`dosen`.`nama` AS `dosen`,`krs`.`kode_makul` AS `kode_makul`,`makul`.`makul` AS `makul`,`krs`.`nim` AS `nim`,`mahasiswa`.`nama` AS `mahasiswa` from ((((`krs` join `kontrak_ngajar`) join `dosen`) join `makul`) join `mahasiswa`) where `makul`.`kode_makul` = `krs`.`kode_makul` and `dosen`.`nidn` = `kontrak_ngajar`.`nidn` and `mahasiswa`.`nim` = `krs`.`nim` and concat(`krs`.`kode_makul`,'-',`krs`.`th_akademik`,'-',`krs`.`semester`) = concat(`kontrak_ngajar`.`kode_makul`,'-',`kontrak_ngajar`.`th_akademik`,'-',`kontrak_ngajar`.`semester`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-05 16:53:06
