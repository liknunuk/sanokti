<?php
session_start();
require "./lib/class.crud.inc.php";


class user extends dbcrud
{
    public function login($u,$p){
        $pass = md5($u."_*_".$p);
        $qry = $this->pickone("*","user","password",$pass);
        
        if($pass == $qry['password']){
            $this->redirect($qry['posisi']);
        }else{
            $_SESSION['pesan'] = "User Tidak Ditemukan";
            header("Location:./");
        }
    }

    private function redirect($pos){
        if($pos == 'mahasiswa' ){
            header("Location:./mahasiswa/");
        }
        if($pos == 'admin' ){
            header("Location:./admin/");
        }
        if($pos == 'dosen' ){
            header("Location:./dosen/");
        }
    }
}


$user = new user;
$user->login($_POST['username'],$_POST['password']);