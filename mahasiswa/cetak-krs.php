<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KRS</title>
    <style>
    table > tbody > tr > td { font-family: monospace; font-size: 12px; }
    </style>
</head>
<body>
    
</body>
</html>

<?php

require('../lib/class.mhs.php');
$mhs = new mhs();

$krs = $mhs->getKrs($_GET['id']);
list($nim,$smt) = explode("-" , $_GET['id']);
$mhsw = $mhs->dataMhs($nim);

echo "
<center><h4>Kartu Rencana Studi</h4></center>
<table width='600' align='center' border='1' cellspacing='0' cellpadding='4'>
    <tr>
        <td width='225'>Tahun Akademik</td><td>{$krs[0]['th_akademik']}</td>
    </tr>
    <tr>
        <td>Nama Mahasiswa</td><td>{$mhsw['nama']}</td>
    </tr>
    <tr>
        <td>NIM</td><td>{$nim}</td>
    </tr>
    <tr>
        <td>Semester</td><td>{$smt}</td>
    </tr>
</table>
<br>
";

//$mkl = explode(",",$krs[0]['mk']);
echo "
<table width='600' align='center' border='1' cellspacing='0' cellpadding='4'>
<tr><th>No.</th><th>Mata Kuliah</th><th>SKS</th></tr>
";
for( $i = 0 ; $i < COUNT($krs) ; $i++ ){
    $nomor = $i + 1;
    // $mk = $admin->dataMakul($krs[$i]['kode_makul']);
    echo "
    <tr><td>{$nomor}</td><td>{$krs[$i]['nmmk']}</td><td align='right'>{$krs[$i]['bobot']} sks</td>
    ";
}
echo "
</table>
"; 

echo "<a href='./?data=krs' style='color:#DDD;'>Kembali</a>";