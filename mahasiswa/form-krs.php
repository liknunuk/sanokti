<?php

require('../lib/class.mhs.php');
$mhs = new mhs();

//$makul = $mhs->allMakul();

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row" style="background-color:darkgray;">
                    <div class="col-sm-12">
                        <h4>Filter Kartu Rencana Studi</h4>
                    </div>
                </div>
                <div class="row" style="background-color:lightgray; padding:15px 0px;">
                    <div class="col-sm-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-6" for="nim">MASUKKAN NIM</label>
                                <div class="col-sm-6">
                                    <input type="text" id="fkrs_nim" class="form-control" placeholder="Nomor Induk Mahasiswa" value="S1TISI30187" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6" for="nim">SEMESTER</label>
                                <div class="col-sm-6">
                                    <input type="number" id="fkrs_smt" class="form-control" placeholder="Semester" value="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right; padding-right: 15px;">
                                    <button class="btn btn-primary" id="cekNimKrs">Periksa</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="hasilCekKRS"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="formKrs" style="display:none;">
        <div class="col-sm-6">
            <h3>Formulir KRS</h3>
            <form action="aksi-krs.php" method="post" class="form-horizontal">
                <div class="form-group">
                    <label for="tha" class="col-sm-3">Thn Akademik</label>
                    <div class="col-sm-9">
                        <input type="number" name="tha" id="krs_tha" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="nim" class="col-sm-3">NIM</label>
                    <div class="col-sm-9">
                        <input type="text" name="nim" id="krs_nim" class="form-control">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="smt" class="col-sm-3">Semester</label>
                    <div class="col-sm-9">
                        <input type="number" name="smt" id="krs_smt" class="form-control">
                    </div>
                </div>

                <div class="form-group" style="display:block;">
                    <label for="mkl" class="col-sm-3">Mata Kuliah</label>
                    <div class="col-sm-9">
                        <textarea name="mkl" rows="3" id="krs_mkl" class="form-control"></textarea>
                    </div>
                </div>
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>Kd Makul</th>
                            <th>Nama Makul</th>
                            <th>Bobot SKS</th>
                            <th>Ket</th>
                        </tr>
                    </thead>
                    <tbody id="dfMkKrs"></tbody>
                    <tbody>
                        <tr>
                            <td colspan="2">Jumlah SKS</td>
                            <td align='right' id="jmsks"></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <div style="text-align:right; padding-right:25px;">
                        <input type="submit" value="Simpan" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-6">
            <h4>Daftar Mata Kuliah</h4>
            <div  style="height:400px; overflow:scroll;">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Kd Makul</th>
                        <th>Nama Makul</th>
                        <th>Bobot SKS</th>
                        <th>Ket</th>
                    </tr>
                </thead>
                <tbody id="listmk"></tbody>
            </table>
            </div>
        </div>
    </div>
</div>


<script>

$(document).ready( function(){
    $("#cekNimKrs").mouseenter( function(){
        let cnim , cmst;
        cnim = $("#fkrs_nim").val(),
        csmt = $("#fkrs_smt").val();
        if(cnim == '' || csmt == ''){
            alert('Lengkapi data!');
        }
    })

    $("#dfMkKrs tr").remove();
    $("#cekNimKrs").click( function(){
        let nim = $("#fkrs_nim").val(),
            smt = $("#fkrs_smt").val();

        $.post('../admin/ds-krs.php' , {
            nim : nim,
            smt : smt,
            mod : 'cl'
        },function(res){
            let data = JSON.parse(res);
            // console.log(data);
            let info = 
            `<p>Mahasiswa: ${data.nama} - Semester: ${data.semester}</p>
            <p>Beban: ${data.beban.toLocaleString('id-ID')} - Terbayar: ${data.terbayar.toLocaleString('id-ID')}</p>
            <p><b class='fz16'>${data.status}</b></p>`;

            $("#hasilCekKRS").html(info);

            if( data.status == "Lunas Administrasi"){
                $("#formKrs").show();
                $("#krs_nim").val( $("#fkrs_nim").val());
                $("#krs_smt").val( parseInt($("#fkrs_smt").val())+1);
                $.getJSON('./cekstatmk.php?nim='+$("#fkrs_nim").val() , function(lmk){
                    $("#listmk tr").remove();
                    $.each( lmk , function(i,data){
                        $("#listmk").append(`
                            <tr>
                            <td><a class='kdmk' style='cursor:pointer;'>${data.kmk}</a></td>
                            <td>${data.mkl}</td>
                            <td>${data.sks}</td>
                            <td>${data.ket}</td>
                            </tr>
                        `)
                    })
                })
            }
            
        })
    })
  
    
    let jmsks = 0;
    $("#listmk").on("click",".kdmk",function(){
        let kdMakul = $(this).text();
        let nmMakul = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        let skMakul = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
        let ktMakul = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
        let sks = parseInt(skMakul);
        jmsks += sks;
        let mkkrs = $("#krs_mkl").val();
        $("#krs_mkl").val( mkkrs+kdMakul+"-"+ktMakul+"-"+skMakul+",");
        
        $("#dfMkKrs").append(`
        <tr>
        <td>${kdMakul}</td>
        <td>${nmMakul}</td>
        <td align='right'>${skMakul}</td>
        <td>${ktMakul}</td>
        </td>
        `);
        $("#jmsks").text(jmsks);
    })
})

function setmkket(data){
    localStorage.setItem('ketmk',data);
}
</script>