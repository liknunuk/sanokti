<?php

require('../lib/class.mhs.php');
$mhs = new mhs();

//$makul = $mhs->allMakul();

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="container-fluid">
                <div class="row" style="background-color:darkgray;">
                    <div class="col-sm-12">
                        <h4>Filter Kartu Hasil Studi</h4>
                    </div>
                </div>
                <div class="row" style="background-color:lightgray; padding:15px 0px;">
                    <div class="col-sm-6">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-6" for="nim">MASUKKAN NIM</label>
                                <div class="col-sm-6">
                                    <input type="text" id="fkrs_nim" class="form-control" placeholder="Nomor Induk Mahasiswa" value="S1TISI30187" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-6" for="nim">SEMESTER</label>
                                <div class="col-sm-6">
                                    <input type="number" id="fkrs_smt" class="form-control" placeholder="Semester" value="10" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12" style="text-align:right; padding-right: 15px;">
                                    <button class="btn btn-primary" id="cekNimKrs">Periksa</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="hasilCekKRS"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:30px;">
        <div class="col-lg-2">&nbsp;</div>
        <div class="col-lg-8">
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode MK</th>
                            <th>Mata Kuliah</th>
                            <th>SKS</th>
                            <th>Nilai</th>
                            <th>Angka</th>
                            <th>K x N</th>
                        </tr>
                    </thead>
                    <tbody  id="dataKhs">
                    
                    </tbody>
                </table>
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td>
                                Total Nilai x Bobot
                            </td>
                            <td class="text-right"><span id="tsxn"></span></td>
                        </tr>
                        <tr>
                            <td>
                                Total Bobot SKS
                            </td>
                            <td class="text-right"><span id="tsks"></span></td>
                        </tr>
                        <tr>
                            <td>
                                Index Prestasi Semester
                            </td>
                            <td class="text-right"><span id="ips"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-lg-2">&nbsp;</div>
    </div>
</div>


<script>

$(document).ready( function(){
    $("#cekNimKrs").mouseenter( function(){
        let cnim , cmst;
        cnim = $("#fkrs_nim").val(),
        csmt = $("#fkrs_smt").val();
        if(cnim == '' || csmt == ''){
            alert('Lengkapi data!');
        }
    })

    $("#dfMkKrs tr").remove();
    $("#cekNimKrs").click( function(){
        let nim = $("#fkrs_nim").val(),
            smt = $("#fkrs_smt").val();

        $.post('../admin/ds-krs.php' , {
            nim : nim,
            smt : smt,
            mod : 'cl'
        },function(res){
            let data = JSON.parse(res);
            // console.log(data);
            let info = 
            `<p>Mahasiswa: ${data.nama} - Semester: ${data.semester}</p>
            <p>Beban: ${data.beban.toLocaleString('id-ID')} - Terbayar: ${data.terbayar.toLocaleString('id-ID')}</p>
            <p><b class='fz16'>${data.status}</b></p>`;

            $("#hasilCekKRS").html(info);

            if( data.status == "Lunas Administrasi"){
                /* data khs goes here */
                let idKontrak = nim+'-'+smt;
                $.getJSON('../admin/ds-khs.php?idk='+idKontrak , function(marks){
                    $("#dataKhs tr").remove();
                    let Nomor=1;
                    $.each( marks.result , function(i,khs){
                        /*no,kodemk,mk,sks,nilai,angka,kxn*/
                        $("#dataKhs").append(`
                        <tr>
                        <td align='right'>${Nomor}.</td>
                        <td>${khs.kmk}</td>
                        <td>${khs.nmk}</td>
                        <td>${khs.sks}</td>
                        <td>&nbsp;</td>
                        <td align='right'>${khs.nil}</td>
                        <td align='right'>${khs.sxn}</td>
                        </tr>
                        `);
                        Nomor+=1;
                    })

                    $("#tsxn").html(marks.tsxn);
                    $("#tsks").html(marks.tsks);
                    $("#ips").html(marks.ips);
                })
            }
            
        })
    })
  
})

</script>