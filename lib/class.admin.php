<?php
require('class.crud.inc.php');
class admin extends dbcrud
{

	/* MATA KULIAH */
	function setmakul($colom,$data){
		$this->insert('makul',$colom,$data);
	}

	//menampilkan daftar makul
	function daftarMakul(){
		$makul = $this->select('*','makul','makul',0);
		return($makul);
	}

	function allMakul(){
		$sql = "SELECT * FROM makul ORDER BY makul";
		$qry = $this->transact($sql);
		$data = [];
		while($res = $qry->fetch()){
			array_push($data , $res );
		}

		return $data;
		$qry = null;
	}

	// data mata kuliah
	function dataMakul($id){
		$data = $this->pickone('*','makul','kode_makul',$id);
		return $data;
	}

	// ubah mata kuliah
	function updateMakul($sets,$data){
		$this->update('makul',$sets,$data,'kode_makul');
	}

	// hapus makul
	function hapusMakul($id){
		$this->delete('makul','kode_makul',$id);
	}




	/* ADMIN */
	// tambah admin
	function setadmin($colom,$data){
		$this->insert('admin_baak',$colom,$data);
	}

	//menampilkan daftar admin
	function daftaradmin(){
		$data = $this->select('*','admin_baak','nama',0);
		return($data);
	}

	function dataAdmin($id){
		$data = $this->pickone('*','admin_baak','nip',$id);
		return($data);
	}

	// ubah admin
	function updateAdmin($sets,$data){
		$this->update('admin_baak',$sets,$data,'nip');
	}

	// hapus admin
	function hapusAdmin($id){
		$this->delete('admin_baak','nip',$id);
	}




	/* DOSEN */
	// tambah dosen
	function setdosen($colom,$data){
		$this->insert('dosen',$colom,$data);
	}

	//menampilkan daftar dosen
	function daftarDosen(){
		$dosen = $this->select('*','dosen','nama',0);
		return($dosen);
	}

	// data dosen
	function dataDosen($id){
		$data = $this->pickone('*','dosen','nidn',$id);
		return $data;
	}

	// ubah dosen
	function updateDosen($sets,$data){
		$this->update('dosen',$sets,$data,'nidn');
	}

	// hapus dosen
	function hapusDosen($id){
		$this->delete('dosen','nidn',$id);
	}

	// tambah kontrak mengajar
	function setKontrakAjar($data){
		$sets = "th_akademik,semester,kode_makul,nidn,id_kontrak";
		$this->insert("kontrak_ngajar",$sets,$data);
	}
	
	function getKontrakAjar($ta){
		$sql = "SELECT ka.id_kontrak , ka.th_akademik , ka.semester , mk.makul , mk.sks , ds.nama , ds.gelar FROM kontrak_ngajar ka, dosen ds, makul mk WHERE ds.nidn = ka.nidn && mk.kode_makul = ka.kode_makul && ka.th_akademik = ? ORDER BY nama,semester";

		$qry = $this->transact($sql,array($ta));
		$data = [];
		while($res = $qry->fetch()){
			array_push( $data , $res );
		}
		return $data;
	}

	function updKontrakAjar($data){
		$sets = "th_akademik,semester,kode_makul,nidn";
		$this->insert("kontrak_ngajar",$sets,$data,'id_kontrak');
	}
	

	/* MAHASISWA */
	function setmahasiswa($colom,$data){
		$this->insert('mahasiswa',$colom,$data);
	}

	//menampilkan daftar mahsiswa
	function daftarMhs(){
		$mhs = $this->select('*','mahasiswa','nim',0);
		return($mhs);
	}

	// data mahasiswa
	function dataMhs($id){
		$data = $this->pickone('*','mahasiswa','nim',$id);
		return $data;
	}
	
	// ubah mahasiswa
	function updateMhs($sets,$data){
		$this->update('mahasiswa',$sets,$data,'nim');
	}

	// hapus mahasiswa
	function hapusMhs($id){
		$this->delete('mahasiswa','nim',$id);
	}




	/* PRODI */
	function setprodi($colom,$data){
		$this->insert('prodi',$colom,$data);
	}

	//menampikan daftar prodi
	function daftarProdi(){
		$prodi = $this->select('*','prodi','id_prodi',0);
		return($prodi);
	}

	//menampikan data prodi
	function dataProdi($id){
		$prodi = $this->pickone('*','prodi','id_prodi',$id);
		return($prodi);
	}
	
	// ubah prodi
	function updateProdi($sets,$data){
		$this->update('prodi',$sets,$data,'id_prodi');
	}

	// hapus prodi
	function hapusProdi($id){
		$this->delete('prodi','id_prodi',$id);
	}



	/* USER */
	// 
	function setuser($colom,$data){
		$this->insert('user',$colom,$data);
	}
		
	// menampilkan daftar user
	function daftaruser(){
		$user = $this->select('*','user','posisi',0);
		return($user);
	}

	function dataUser($id){
		$data = $this->pickone('*','user','id_user',$id);
		return $data;
	}

	function updateUser($sets,$data){
		$this->update('user',$sets,$data,'id_user');
	}

	function hapusUser($id){
		$this->delete('user','id_user',$id);
	}


	
	
	/* ADMINISTRASI */
	// pembayaran administrasi
	function bayarAdmin($kolom,$data)
	{
		$adm = $this->insert('administrasi',$kolom,$data);
		
	}

	function nomorNotaBaru(){
		$now = date('ymd');
		$sql = "SELECT COUNT(id_biaya) AS id_biaya FROM administrasi WHERE id_biaya LIKE '{$now}%' ";
		$qry = $this->transact($sql);
		$res = $qry->fetch();
		$data = $res['id_biaya'];
		if($data < 1 ){
			$idx = '0001';
			$idb = $now.$idx;
		}else{
			$idx = $data + 1;
			$idb = $now.sprintf('%04d',$idx);
		}
		return $idb;
		$qry = null;
	}

	function lapbayar($bulan){
		$sql = "SELECT DATE_FORMAT(tgl_pembayaran , '%d-%m-%Y') tanggal , administrasi.nim , mahasiswa.nama mahasiswa, administrasi.jumlah , administrasi.id_biaya , admin_baak.nama admin FROM administrasi , admin_baak , mahasiswa WHERE mahasiswa.nim = administrasi.nim && admin_baak.nip = administrasi.nip && tgl_pembayaran LIKE '".$bulan."%' ORDER BY tgl_pembayaran DESC";
		$qry = $this->transact($sql);
		$data = [];
		while ($res = $qry->fetch()){
			array_push($data,$res);
		}
		return $data;
	}

	function riwayatbayar($nim){
		$sql = "SELECT DATE_FORMAT(tgl_pembayaran , '%d-%m-%Y') tanggal , administrasi.nim , mahasiswa.nama mahasiswa, administrasi.jumlah , administrasi.id_biaya , admin_baak.nama admin FROM administrasi , admin_baak , mahasiswa WHERE mahasiswa.nim = administrasi.nim && admin_baak.nip = administrasi.nip && administrasi.nim = '".$nim."' ORDER BY tgl_pembayaran DESC";
		$qry = $this->transact($sql);
		$data = [];
		while ($res = $qry->fetch()){
			array_push($data,$res);
		}
		return $data;
	}


	/* BEBAN BIAYA */

	function setbiaya($sets,$data){
		$this->insert('beban_biaya',$sets,$data);
	}

	function daftarbiaya(){
		$sql = "SELECT beban_biaya.* , prodi.nama prodi FROM beban_biaya , prodi WHERE prodi.id_prodi = beban_biaya.jurusan ORDER BY th_akademik , jurusan , semester LIMIT 25";
		$qry = $this->transact($sql);
		$data = [];
		while ($res = $qry->fetch()){
			array_push($data,$res);
		}
		return $data;
	}

	function databiaya($id){
		$data = $this->pickone("*","beban_biaya","idx",$id);
		return $data;
	}

	function updatebiaya($sets,$data){
		$this->update('beban_biaya',$sets,$data,'idx');
	}

	function hapusbiaya($idx){
		$this->delete('beban_biaya','idx',$idx);
	}

	/* KRS */
	// cek lunas biaya
	function cekLunas($nim,$smt){
		$sql = "SELECT administrasi.nim, mahasiswa.nama, mahasiswa.id_prodi prodi , sum(administrasi.jumlah) terbayar , beban_biaya.jumlah beban , beban_biaya.satuan FROM administrasi , beban_biaya , mahasiswa WHERE beban_biaya.semester = ${smt} && mahasiswa.nim ='{$nim}' && beban_biaya.jurusan = mahasiswa.id_prodi && administrasi.nim = mahasiswa.nim && beban_biaya.semester = administrasi.semester GROUP BY nim,administrasi.semester;";

		$qry = $this->transact($sql);
		$res = $qry->fetch();
		if($res['satuan'] == 'per semester' && $res['prodi']=='si' ){
			$ceklunas = $res['beban'];
		}elseif($res['satuan'] == 'per bulan' && $res['prodi']=='ti'){
			$ceklunas = $res['beban'] * 6;
		}else{
			$ceklunas = 0;
		}
		if ($res['terbayar'] >= $ceklunas && $ceklunas > 0 ){
			$status = "Lunas Administrasi";
		}else{
			$status = "Belum Lunas Administrasi";
		}
		$res['beban'] = $ceklunas;
		$res['semester'] = $smt;
		$res['status'] = $status;
		$res['terbayar'] = (int)$res['terbayar'];

		return $res;
	}

	function setKrs($sets,$data){
		$this->insert( "kkrs" , $sets , $data );
	}

	function getIdKhrs($nim){
		$idk = $this->picksome("MAX(RIGHT(id_kontrak))" , "krs" , "id_kontrak LIKE '{$nim}%'");
		return $idk;
	}

	function setkhrs($data){
		$sql = "INSERT INTO krs SET  nim = ? , semester = ? , kode_makul = ? , id_kontrak = ? , id_khrs = ? ";
	}

	function getKrs($id){
		$data = $this->picksome("*","kkrs","kdKrs='{$id}'");
		return $data;
	}

}
?>
