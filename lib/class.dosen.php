<?php
require('class.crud.inc.php');
class dosen extends dbcrud
{
    function weare(){
        $we = $this->select("nidn,nama","dosen","nama","0");
        return $we;
    }

    function wemulang($ta,$sm,$ds){
        $sql = "SELECT kontrak_ngajar.kode_makul kdmk, makul.makul nmmk FROM kontrak_ngajar , makul WHERE makul.kode_makul = kontrak_ngajar.kode_makul && kontrak_ngajar.th_akademik = ? && kontrak_ngajar.semester = ? && nidn = ?";
        $qry = $this->transact($sql, array($ta,$sm,$ds));
        $data = [];
        while($res = $qry->fetch()){
            array_push( $data , $res );
        }
        return $data; $qry = null;
    }

    function ourstudents($ta,$sm,$ds,$mk){
        $sql = "SELECT id_krhs , nim,mahasiswa FROM v_kbm WHERE nidn=? && th_akademik = ? && semester = ? && kode_makul = ? ";

        $qry = $this->transact($sql, array($ds, $ta, $sm , $mk));
        $data = [];
        while($res = $qry->fetch()){
            array_push( $data , $res );
        }
        return $data; $qry = null;
    }

    function setMark($idkrs,$mark){
        $this->update("krs","nilai",array($mark,$idkrs),'id_krhs');
    }
}
?>
