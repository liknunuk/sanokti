<?php
require('class.crud.inc.php');
class mhs extends dbcrud
{

	/* MATA KULIAH */
	function setmakul($colom,$data){
		$this->insert('makul',$colom,$data);
	}

	//menampilkan daftar makul
	function daftarMakul(){
		$makul = $this->select('*','makul','makul',0);
		return($makul);
	}

	function allMakul(){
		$sql = "SELECT * FROM makul ORDER BY makul";
		$qry = $this->transact($sql);
		$data = [];
		while($res = $qry->fetch()){
			array_push($data , $res );
		}

		return $data;
		$qry = null;
	}

	// data mata kuliah
	function dataMakul($id){
		$data = $this->pickone('*','makul','kode_makul',$id);
		return $data;
	}

	

	/* MAHASISWA */
	
	// data mahasiswa
	function dataMhs($id){
		$data = $this->pickone('*','mahasiswa','nim',$id);
		return $data;
	}
	
	// ubah mahasiswa
	function updateMhs($sets,$data){
		$this->update('mahasiswa',$sets,$data,'nim');
	}

	
	/* PRODI */
	//menampikan daftar prodi
	function daftarProdi(){
		$prodi = $this->select('*','prodi','id_prodi',0);
		return($prodi);
	}

	//menampikan data prodi
	function dataProdi($id){
		$prodi = $this->pickone('*','prodi','id_prodi',$id);
		return($prodi);
	}
		
	
	function riwayatbayar($nim){
		$sql = "SELECT DATE_FORMAT(tgl_pembayaran , '%d-%m-%Y') tanggal , administrasi.nim , mahasiswa.nama mahasiswa, administrasi.jumlah , administrasi.id_biaya , admin_baak.nama admin FROM administrasi , admin_baak , mahasiswa WHERE mahasiswa.nim = administrasi.nim && admin_baak.nip = administrasi.nip && administrasi.nim = '".$nim."' ORDER BY tgl_pembayaran DESC";
		$qry = $this->transact($sql);
		$data = [];
		while ($res = $qry->fetch()){
			array_push($data,$res);
		}
		return $data;
	}


	/* BEBAN BIAYA */

	
	function daftarbiaya(){
		$sql = "SELECT beban_biaya.* , prodi.nama prodi FROM beban_biaya , prodi WHERE prodi.id_prodi = beban_biaya.jurusan ORDER BY th_akademik , jurusan , semester LIMIT 25";
		$qry = $this->transact($sql);
		$data = [];
		while ($res = $qry->fetch()){
			array_push($data,$res);
		}
		return $data;
	}

	/* KRS */
	// cek lunas biaya
	function cekLunas($nim,$smt){
		$sql = "SELECT administrasi.nim, mahasiswa.nama, mahasiswa.id_prodi prodi , sum(administrasi.jumlah) terbayar , beban_biaya.jumlah beban , beban_biaya.satuan FROM administrasi , beban_biaya , mahasiswa WHERE beban_biaya.semester = $smt && mahasiswa.nim ='".$nim."' && beban_biaya.jurusan = mahasiswa.id_prodi && administrasi.nim = mahasiswa.nim ";

		$qry = $this->transact($sql);
		$res = $qry->fetch();
		if($res['satuan'] == 'per semester' && $res['prodi']=='si' ){
			$ceklunas = $res['beban'];
		}elseif($res['satuan'] == 'per bulan' && $res['prodi']=='ti'){
			$ceklunas = $res['beban'] * 6;
		}else{
			$ceklunas = 0;
		}
		if ($res['terbayar'] >= $ceklunas && $ceklunas > 0 ){
			$status = "Lunas Administrasi";
		}else{
			$status = "Belum Lunas Administrasi";
		}
		$res['beban'] = $ceklunas;
		$res['semester'] = $smt;
		$res['status'] = $status;
		$res['terbayar'] = (int)$res['terbayar'];

		return $res;
	}

	function setKrs($sets,$data){
		$this->insert( "kkrs" , $sets , $data );
	}

	function getIdKhrs($nim){
		$idk = $this->picksome("MAX(RIGHT(id_krhs,3)) id_krhs" , "krs" , "id_kontrak LIKE '{$nim}%'");
		return $idk;
	}

	function setkhrs($data){
        $sql = "INSERT INTO krs SET  nim = ? , semester = ? , kode_makul = ? , bobot = ? , th_akademik = ? , id_kontrak = ? , keterangan = ? , id_krhs = ? ";
        $qry = $this->transact($sql , $data);
	}

	function getKrs($id){
		$data = $this->picksome("krs.* , makul.makul nmmk","krs,makul","id_kontrak='{$id}' && makul.kode_makul = krs.kode_makul");
		return $data;
	}

	function cekMkKet($kdmk,$nim){
		$sql = "SELECT COUNT(kode_makul) AS mk FROM krs WHERE nim = ? && kode_makul = ?";
		$qry = $this->transact($sql , array($nim,$kdmk));
		$res = $qry->fetch();
		$ket = $res['mk'] == 0 ? 'baru':'mengulang';
		return($ket);
	}

	function mkkrs($nim){
		$sql = "SELECT kode_makul , makul , sks FROM makul ORDER BY kode_makul ";
		$qry = $this->transact($sql);
		$makul = [];
		while( $mk = $qry->fetch()){
			$ket = $this->cekMkKet($mk['kode_makul'],$nim);
			$data = array('kmk'=>$mk['kode_makul'],'mkl'=>$mk['makul'],'sks'=>$mk['sks'],'ket'=>$ket);
			array_push($makul,$data);
		}

		return $makul;
	}

}
?>
