DROP TABLE IF EXISTS `form_iface`;
CREATE TABLE `form_iface` (
  `urut` int(3) NOT NULL AUTO_INCREMENT,
  `fgroup` varchar(20) DEFAULT NULL,
  `pertanyaan` varchar(50) DEFAULT NULL,
  `formName` varchar(20) DEFAULT NULL,
  `formType` enum('blank','text','textarea','password','select','date','file') DEFAULT 'text',
  `formId` varchar(20) DEFAULT NULL,
  `fparam` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`urut`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `form_opsi`;
CREATE TABLE `form_opsi` (
  `idx_pilihan` int(2) NOT NULL AUTO_INCREMENT,
  `optGroup` varchar(20) DEFAULT NULL,
  `optValue` varchar(20) DEFAULT NULL,
  `optLabel` text,
  PRIMARY KEY (`idx_pilihan`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
