DROP DATABASE IF EXISTS `sistem_akademik`;
CREATE DATABASE `sistem_akademik`;
CREATE USER nokti@localhost IDENTIFIED BY 'skripsinokti';
GRANT ALL PRIVILEGES ON `sistem_akademik`.* TO nokti@localhost WITH GRANT OPTION;
FLUSH PRIVILEGES;