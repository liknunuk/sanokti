<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/siskadem.css">
    <title>Sistem Akademik STIMIK Tunas Bangsa</title>
</head>
<body class='bg-soft-green'>
    <div class="row jumbotron">
        <div class="col-lg-2">
            <div id="logo-holder">
                <img src="img/stb-logo.png" alt="" srcset="">
            </div>
        </div>
        <div class="col-lg-10 text-center">
            <h3>SEKOLAH TINGGI MANAGEMEN INFORMATIKA DAN KOMPUTER TUNAS BANGSA</h3>
            <h1>SISTEM INFORMASI AKADEMIK</h1>
        </div>
    </div>

    <div class="row bg-soft-green">
        <div class="col-lg-4">&nbsp;</div>
        <div class="col-lg-4" id="login-area">
            <?php
                if(isset($_SESSION['pesan'])){
                    echo '
                    <div class="login-failed bg-danger" style="height: 40px; text-align:center; color: black; font-size: 24px;">'.$_SESSION['pesan'].'</div>
                    ';
                }
                unset($_SESSION['pesan']);
            ?>
            <div class="login-wrapper">
                <div class="login-header">
                    <h3>LOGIN AREA</h3>
                </div>
                
                
                <div class="login-form">

                    <form action="login.php" method="post">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="email" type="text" class="form-control" name="username" placeholder="username">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div class="form-group text-right" style="padding-right: 10px;">
                        <input type="submit" value="Login" class="btn btn-success">
                    </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-lg-4">&nbsp;</div>
    </div>
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</body>
</html>